# Funções nativas do Python

# -------------------------------------------------------------------------------------------------------------------

# Function abs()
"""

abs(n) -> Retorna o valor absoluto de um número
n -> Um número

"""

a = abs(-7.25)
print(a)
# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function all()
"""

all(iterable) -> Retorna True se todos os itens de um iterável forem 'true'
iterable -> Um iterável

"""

mylist = [0, 1, 1]
b = all(mylist)
print(b)
# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function any()
"""

any(iterable) -> Retorna True se algum item de um iterável for 'true'
iterable -> Um iterável

"""

mylist = [False, True, False]
c = any(mylist)
print(c)
# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function ascii()
"""

ascii(object) -> Substitui caracteres não existentes da tabela ascii com algum símbolo
object -> Um objeto qualquer (Uma string, uma tupla, lista ou qualquer outro)

"""

d = ascii("âéüà")
print(d)
# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function bin()
"""

bin(n) ->  Retorna o valor em binário de um número especificado no parâmetro da função
n -> Um número inteiro

"""

e = bin(3)
print(e)
# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function bool()

"""

bool(object) -> Retorna um booleano de um objeto especificado
object -> Um objeto qualquer (string, numero, tupla, lista, dicionário e etc.)

OBS: o objeto sempre vai ser True exceto se:
    - For vazio [], (), {}
    - For 'False'
    - For 0
    - For 'None'
"""

f = bool(1)
print(f)
# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function breakpoint()

"""
breakpoint(*args, **kws)

Chama o objeto pdb.set_trace() para criar um ponto de debugger
*args / *kws -> Argumentos para critério de debugger

"""
# Fim

# -------------------------------------------------------------------------------------------------------------------


# Function bytearray()

"""

bytearray(x, encoding, error) -> Retorna um array de bytes
x -> Usado para criar o array de bytes. Se for um inteiro, um array de bytes vazio com o tamanho especificado será criado
    Se for uma string, é necessário passar a codificação da string (ascii, utf-8 e etc)

encoding -> A codificação da string (utf-8, ascii e etc.)
error -> Especificar o que fazer se um erro com o parâmetro 'encoding' falhar

"""
g = bytearray(4)
print(g)
# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function bytes()

"""

bytes(x, encoding, error) -> Retorna um objeto de bytes
Parâmetros -> Mesmos que o bytearray()

"""
h = bytes(6)
print(h)
# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function callable()

"""

callable(object) -> Retorna True se o objeto especificado por parâmetro for 'chamável'
object -> Um objeto qualquer

"""


def i(i): return i * 2


print(callable(i))
# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function chr()

"""

chr(number) -> Retorna o unicode de um número especificado por parâmetro
number -> Um inteiro representando um unicode (Ex.: 97 - a)

"""
j = chr(97)
print(j)
# Fim

# -------------------------------------------------------------------------------------------------------------------
# Function @classmethod

"""
Transforma um método em um método de classe. É um decorator

"""


class Teste:
    @classmethod
    def teste(cls, arg1, arg2):
        pass

# Fim

# -------------------------------------------------------------------------------------------------------------------


# -------------------------------------------------------------------------------------------------------------------

# Function enumerate()

"""

enumerate(iterable, start) -> Enumera um iterável
iterable -> Um iterável
start -> Índice de onde começará a enumerar o iterável (Se não especificado, o default é o índice 0)

"""
enumerate([a for a in ("a", "b", "c", "d")])
# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function filter()

"""

filter(function, iterable) -> Filtra um iterável de acordo com critério determinado no parâmetro function
function -> Função a ser utilizada para filtrar os dados

"""

# Função lambda verifica números e filtra somente os ímpares
impares = filter(lambda x: x % 2 != 0, [5, 12, 17, 18, 24, 32])
print(set(impares))

# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function map()

"""

map(function, iterables) -> Executa uma função para cada item nos iteráveis passados por parâmetro.

"""
# Função multiplica os números de um iterável pelos números de outro iterável

try:
    mult = map(lambda a, b: a * b, [2, 3, 5, 6], (4, 7, 8, 9))
    print(sorted(mult))
except Exception:
    print("Não foi possível concluir a operação!")

# OBS: Se os iteráveis forem de tamanhos distintos, serão retornados a quantidade de itens contidas no menor iterável
# Ex: Se o iterável A tem 3 itens e o iterável B tem 4 itens, o 4º item do iterável B é descartado e a operação só é feita com os 3 primeiros

# Fim

# -------------------------------------------------------------------------------------------------------------------

# Function getattr()

"""

getattr(object, attribute, default) -> Retorna o atributo de uma classe
object -> O objeto onde será buscado o atributo
attribute -> O atributo a ser buscado no objeto especificado
default -> Opcional. Valor retornado se nenhum atributo for especificado ou o atributo especificado não existir

"""


class Pessoa:
    nome = "Jv"
    idade = "22 anos"
    teste = "teste"


try:
    gt = getattr(Pessoa, 'sexo', "padrão")
except Exception:
    print("Não encontrado")

# Fim

# -------------------------------------------------------------------------------------------------------------------
