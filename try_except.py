# Criando e manipulando exceções com Python

#Tentando imprimir variável não existente
try:
    print(x)
except NameError:
    print("A variável especificada não existe")
except:
    print("Alguma coisa deu errado, tente novamente!")
#Fim


#Utilizando else caso nenhuma exceção ocorra
try:
    print("Olá")
except:
    print("Um erro ocorreu!")
else:
    print("Nenhum erro ocorreu")
#Fim


#Utilizando o finally para executar um comando independentemente se ocorrer uma exceção ou não
try:
    print(d)
except:
    print("Ocorreu um erro")
finally:
    print("Finally executado")
    
#Exemplo prático de como utilizar o finally

try:
    f = open("file.txt")
    f.write("Um texto qualquer")
except:
    print("Não foi possível escrever no arquivo")
finally:
    f.close() #Fechando o arquivo independentemente se houver uma exceção ou não

#Fim


#Criando novas exceções utilizando raise

lista = ["a", "b", 2, "c", "d", 5]

#Verificando se a lista só contém strings. Se existir outro tipo, gera uma exceção
for x in lista:
    if type(x) is not "str":
        raise Exception("Somente caracteres literais são permitidos nesta lista!")
    else:
        print("{a}".format(a=x))
#Fim

"""
Exceções nativas do Python

'BaseException' -> Exceção base para as demais
'Exception' -> Exceção geral
'ArithmeticError' -> Exceção base para erros de aritmética como divisão or zero e etc.
'BufferError'
'LookupError'
'AssertionError'
'AttributeError'
'EOFError'
'FloatingPointError'
'GeneratorExit'
'ImportError'
'ModuleNotFoundError'
'IndexError'
'KeyError'
'KeyboardInterrupt'
'MemoryError'
'NameError'
'NotImplementedError'
'OSError'
'OverflowError'
'RecursionError'
'ReferenceError'
'RuntimeError'
'StopIteration'
'StopAsyncIteration'
'SyntaxError'
'IndentationError'
'TabError'
'SystemError'
'SystemExit'
'TypeError' -> Se o tipo da variável for incompatível
'UnboundLocalError'
'UnicodeError'
'UnicodeEncodeError'
'UnicodeDecodeError'
'UnicodeTranslateError'
'ValueError'
'ZeroDivisionError'
'EnvironmentError'
'IOError'
'WindowsError'
'BlockingIOError'
'ChildProcessError'
'ConnectionError'
'BrokenPipeError'
'ConnectionAbortedError'
'ConnectionRefusedError'
'ConnectionResetError'
'FileExistsError'
'FileNotFoundError'
'InterruptedError'
'IsADirectoryError'
'NotADirectoryError'
'PermissionError'
'ProcesslookupError'
'TimeoutError'

"""



"""

Hierarquia de exceções

BaseException
 +-- SystemExit
 +-- KeyboardInterrupt
 +-- GeneratorExit
 +-- Exception
      +-- StopIteration
      +-- StopAsyncIteration
      +-- ArithmeticError
      |    +-- FloatingPointError
      |    +-- OverflowError
      |    +-- ZeroDivisionError
      +-- AssertionError
      +-- AttributeError
      +-- BufferError
      +-- EOFError
      +-- ImportError
      |    +-- ModuleNotFoundError
      +-- LookupError
      |    +-- IndexError
      |    +-- KeyError
      +-- MemoryError
      +-- NameError
      |    +-- UnboundLocalError
      +-- OSError
      |    +-- BlockingIOError
      |    +-- ChildProcessError
      |    +-- ConnectionError
      |    |    +-- BrokenPipeError
      |    |    +-- ConnectionAbortedError
      |    |    +-- ConnectionRefusedError
      |    |    +-- ConnectionResetError
      |    +-- FileExistsError
      |    +-- FileNotFoundError
      |    +-- InterruptedError
      |    +-- IsADirectoryError
      |    +-- NotADirectoryError
      |    +-- PermissionError
      |    +-- ProcessLookupError
      |    +-- TimeoutError
      +-- ReferenceError
      +-- RuntimeError
      |    +-- NotImplementedError
      |    +-- RecursionError
      +-- SyntaxError
      |    +-- IndentationError
      |         +-- TabError
      +-- SystemError
      +-- TypeError
      +-- ValueError
      |    +-- UnicodeError
      |         +-- UnicodeDecodeError
      |         +-- UnicodeEncodeError
      |         +-- UnicodeTranslateError
      +-- Warning
           +-- DeprecationWarning
           +-- PendingDeprecationWarning
           +-- RuntimeWarning
           +-- SyntaxWarning
           +-- UserWarning
           +-- FutureWarning
           +-- ImportWarning
           +-- UnicodeWarning
           +-- BytesWarning
           +-- ResourceWarning


"""