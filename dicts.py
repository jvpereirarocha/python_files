import datetime

#Formas de se declarar um dicionário
thisdict = {
    "brand": "Ford",
    "model": "Mustang",
    "year": datetime.datetime.now().year
}

thisdict["color"] = "Red"

thisdict = dict(brand="Ford", model="Mustang", year=datetime.datetime.now().year, color="Red")

#Fim

# Listando o dicionário
print(thisdict) 
#Fim

# Acessando itens do dicionário
thisdict["model"]
thisdict.get("model")
# Fim

#Mudando valor dos itens do dicionário
thisdict["year"] = 2019
#Fim

#Formas de iteração em um dicionário para retornar os valores
for x in thisdict:
    print("Dict: ", thisdict[x]) 

for x in thisdict.values():
    print("D:" , x)
#Obs: Retorna somente os valores do dicionário
#Fim

#Formas de iteração em um dicionário para retornar as chaves
for x in thisdict:
    print("Keys: ", x)
    
for x in thisdict.keys():
    print("K: ", x) 
#Obs: Retorna somente as chaves do dicionário
#Fim

#Removendo itens
thisdict.pop("brand") #Forma 1
thisdict.popitem() #Remove aleatoriamente
del thisdict["color"] #Para remover um item específico
del thisdict #Remove o dicionário inteiro
thisdict.clear() #Esvazia o dicionário
print("Novo dict: ", thisdict)
#Fim

#Copiar dicionário
mydict = thisdict.copy()
mydict = dict(thisdict)
print("My dict: ", mydict)
#Fim

#Métodos relacionados a dicionários
clear()
copy()
fromkeys() #Insere um valor para especificadas chaves.
get()
items()
keys()
pop()
popitem()
setdefault()
update()
values()
#Fim

