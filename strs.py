nome = "joao Victor"
print(nome.capitalize()) #Transforma a primeira letra da string em maíuscula

txt = "TESTE"
print(txt.casefold()) #A string fica toda minúscula

cent = "tecfkfjfj"
print(cent.center(15)) # Printa a string com o espaço de (Nesse caso 15) caracteres no terminal

coun = "Um teste, dois teste, três teste"
print(coun.count("teste")) # Retorna o número de vezes que aquele valor especificado foi escrito na string

enc = "hhjjü"
print(enc.encode(encoding="ascii", errors="namereplace")) # Altera o tipo de codificação da string. Default: UTF-8

"""
    Lista de erros:
    'backslashreplace',
    'ignore',
    'namereplace',
    'strict',
    'replace',
    'xmlcharrefreplace'
"""

ends = "Este é um texto de teste."
value = "t"
start = 0
end = (len(ends) - 1)
print(ends.endswith(value, start, end))

""" value -> Valor a ser checado
    start -> Em qual caractere da string deve iniciar a busca
    end -> Em qual caractere da string deve finalizar a busca
"""

exp = "H\te\tl\tl\to\t"
print(exp.expandtabs(6))

t = "texto"
start = 0
end = len(t) - 1
print(t.find("t", start, end))
print("Posição: {pos} ".format(pos=(t.find("t") + 1)))

alnum = "UmTextocomNumero12"
print(alnum.isalnum()) #Verifica se o texto da string possui letras e números somente

alf = "Umvffttassa"
print(alf.isalpha()) #Verifica se o texto da string possui só letras

dec = "671234"
print("Decimal: ", dec.isdecimal()) #Verifica se o texto da string possui só numeros

dig = " "
print("Digit: ", dig.isdigit()) #Verifica se o texto da string possui só numeros

lst = ["a", "b", "c", "d", "e", "f"]
print("-".join(lst))

r = "One, Two, Four"
print(r.replace("Four", "Three")) # Substituindo o texto 'old' para 'new' .replace(old, new)

rows = "Welcome To The\n Jungle"
print(rows.splitlines())

s = "Welcome-to-the-jungle"
print(s.split("-")) # Quebra a string em uma lista

tlt = "joao victor pereira rocha"
print(tlt.title())

zf = "40"
print(zf.zfill(10)) #Insere 0 à esquerda (o Número 40 começa no 10º dígito)

st = "      teste    "
print ("Este é um %s" %st.strip())

ff = "TEXTO"
tt = "text"

print("Lower: ", ff.lower())
print("Upper: ", tt.upper())
