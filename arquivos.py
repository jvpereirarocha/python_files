# Manipulação de arquivos

# -*- coding: utf-8 -*-

"""
'r' -> Read
'a' -> Append
'w' -> Write
'x' -> Create

't' -> Text
'b' -> Binary
"""

# Abrindo e lendo um arquivo
import os
f = open("file.txt")  # Abrir o arquivo
f = open("file.txt", "r")  # Permite somente leitura do arquivo utilizando 'r' como 2º parâmetro
# print(f.read()) #Mostra todo o conteúdo do arquivo
f = open("file.txt", "r")  # Permite somente leitura do arquivo utilizando 'r' como 2º parâmetro
x = 5
# print(f.read(x)) #Mostra o conteúdo do arquivo de acordo com o número de caracteres passado no parâmetro
f = open("file.txt", "r")
# print(f.readline()) #Lê uma linha do arquivo

# Iterando em todas as linhas de um arquivo
f = open("file.txt", "r")
for x in f:
    print(x)

# Fim


# Escrevendo em um arquivo
f = open("file.txt", "a")  # Insere (append) conteúdo no final do arquivo
f.write("Última linha inserida\n")

f = open("file.txt", "w")  # Apaga todo o conteúdo existente do arquivo e sobrescreve o mesmo
f.write("Escrevendo linhas")
# Fim


# Fechando um arquivo
f = open("file.txt", "r")
f.close()
# Fim

# Criando um arquivo
f = open("arquivo.txt", "x")  # Cria um arquivo (Retorna um erro se o arquivo já existir)
# Cria um arquivo caso o mesmo não exista (NÃO retorna erro se o arquivo já existir)
f = open("arquivo.txt", "w")
# Cria um arquivo caso o mesmo não exista (NÃO retorna erro se o arquivo já existir)
f = open("arq.txt", "a")
f.close()
# Fim

# Removendo um arquivo
os.remove("arq.txt")

# Verificando se o arquivo existe

if os.path.exists("arquivo.txt"):
    os.remove("arquivo.txt")
else:
    print("O arquivo não existe")


# Deletando uma pasta inteira
os.rmdir("minhaPasta")

# Fim
