# Utilização de expressões regulares em Python

import re

txt = "The rain in Spain"
x = re.search("^The.*Spain$", txt)
print(x)

#Funções de RegEx

"""
findall - > Retorna uma lista contendo todos os resultados encontrados
search -> Retorna um objeto do tipo Match se for encontrado algum resultado na busca
split -> Retorna uma lista onde a strings são quebradas em cada 'match'
sub -> Substitui um ou vários 'matchs' com uma string
"""

#Fim


#Metacaracteres para buscas e seus significados

"""
[]  -> Um 'set com os caracteres' (Ex.:[a-m])
\   -> Retorna uma sequência especificada (Ex.: \d)
.   -> Qualquer caractere (Com exceção de nova linha) - Ex.: he..o
^   -> Se a sequência começa com (Ex.: ^hello)
$   -> Se a sequência termina com (Ex.: world$)
*   -> Nenhuma ou mais ocorrências (Ex.: aix*)
+   -> Uma ou mais ocorrências (Ex.: aix+) 
{}  -> Exatamente o número especificado de ocorrências (Ex.: al{2})
|   -> Ou (Ex.: falls|stays)
()  -> Pega e agrupa
"""

#Fim


#Sequências especiais
"""

\A -> Retorna um 'match' se os caracteres especificados estiverem no começo da string
        - Ex.: \AThe
        
\b -> Retorna um 'match' se os caracteres especificados estiverem no começo ou no final da string
        - Ex.: r"\bain" ou r"ain\b"
        
\B -> Retorna um 'match' se os caracteres especificados existirem mas NÃO no começo ou no final da string
        - Ex.: r"\Bain" ou r"ain\B"
        
\d -> Retorna um 'match' se nos caracteres especificados tiverem dígitos (0 a 9)
        - Ex.: "\d"
        
\D -> Retorna um 'match' se nos caracteres especificados NÃO existirem dígitos (0 a 9)
        - Ex.: "\D"
        
\s -> Retorna um 'match' se na string existirem espaços em branco
        - Ex.: "\s"
        
\S -> Retorna um 'match' onde NÃO existem espaços em branco na string
        - Ex.: "\S"
        
\w -> Retorna um 'match' onde existam caracteres de palavras (A-Z, 0 A 9, _) na string
        - Ex.: "\w"
        
\W -> Retorna um 'match' onde NÃO existem caracteres de palavras (A-Z, 0 A 9, _) na string
        - Ex.: "\W"
        
\Z -> Retorna um 'match' se os caracteres especificados estiverem no final da string
        - Ex.: "Spain\Z"

"""
#Fim


#Sets
"""

[arn]   -> Retorna um 'match' onde um dos caracteres especificados estiverem presentes (Neste caso a, r ou n) 
[a-n]   -> Retorna um 'match' para qualquer caractere minúsculo, ordenado alfabeticamente entre a e n (neste exemplo)
[^arn]   -> Retorna um 'match' para qualquer caractere EXCETO os caracteres especificados (neste caso a, r ou n)
[0123]   -> Retorna um 'match' onde forem encontrados os dígitos especificados (neste caso 0, 1, 2 ou 3)
[0-9]   -> Retorna um 'match' para qualquer dígito entre 0 e 9
[0-5][0-9]  -> Retorna um 'match' para dois dígitos de 00 e 59
[a-zA-Z]    -> Retorna um 'match' para cada caractere alfabético entre a e z minúsculo ou maiúsculo
[+] -> Retorna um 'match' para qualquer caractere + (neste caso) na string

"""
#Fim


# Métodos de exemplo

"""
    Exemplo utilizando findall() 
    Retorna uma lista com o intervalo de caracteres especificado.
    Retorna uma lista vazia se o intervalo de caracteres não for encontrado

"""

import re
str = "The rain in Spain"
a = re.findall("in", str)
print(a)


"""
    Exemplo utilizando search()
"""

import re
str = "The rain in Spain"
b = re.search("\s", str)
print(b)

#Fim