# -*- coding: utf-8 -*-

# Exercícios com Listas #

vetor = []

# Exercício1:Faça um Programa que leia um vetor de 5 números inteiros e mostre-os #


def lerVetorCinco(vetor):
    for x in range(0, 5):
        vetor.append(int(input("Digite um número: ")))
    return vetor


# print(lerVetorCinco(vetor))


# Fim do Exercício 1 #


# Exercicio2: Faça um Programa que leia um vetor de 10 números reais e mostre-os na ordem inversa

def lerVetorDezInverso(vetor):
    for x in range(0, 10):
        vetor.append(int(input("Digite um número: ")))
    vetor.reverse()
    return vetor


# print("{}".format(lerVetorDezInverso(vetor)), end='\n')


# Fim do exercício 2


# Exercicio3: Faça um Programa que leia 4 notas, mostre as notas e a média na tela. #

def lerQuatroNotas():
    notas = []
    soma = 0
    media = 0
    for x in range(0, 4):
        notas.append(float(input("Digite a nota {}: ".format(x+1))))
        soma += notas[x]
    media = soma / len(notas)
    return media


# print(lerQuatroNotas())

# Fim do exercício 3


# Exercicio4: Faça um Programa que leia um vetor de 10 caracteres, e diga quantas consoantes foram lidas. Imprima as consoantes. #

def lerVetorConsoantes():
    x = []
    for i in range(0, 3):
        x.append(str(input()))
    return x


print(lerVetorConsoantes())

# Exercicio5: Inicio


def vetoresParImpar():
    vetor = []
    for i in range(0, 5):
        vetor.append(int(input("Digite um numero: ")))

    par = [x for x in vetor if x % 2 == 0]
    impar = [y for y in vetor if y % 2 != 0]

    print("Vetor: ", vetor)
    print("Pares: ", par)
    print("Impares: ", impar)


# vetoresParImpar()

# Fim do Exercicio 5


def xrange(start, stop, step=1):
    while start < stop:
        yield start
        start += step


xrange(1, 20)
