thislist = ["apple", "banana", "cherry", "orange", "mango", "kiwi", "melon"]
print(thislist) # Imprimindo a lista

#Acessando itens da lista
print(thislist[0]) #Primeiro índice da lista
print(thislist[-1]) #Imprime a lista na ordem reversa
# Fim

#Determinando 'range' de acesso na lista
print(thislist[1:4]) #Acessa os itens que estão da posição 1 até a posição 4
print(thislist[2:14]) #Se ultrapassar os limites da lista, imprime ela toda a partir da posição 2
print(thislist[:4]) # Imprime os 4 itens do começo da lista excluindo o resto no final
print(thislist[2:]) # Imprime os itens excluindo os 2 primeiros da lista (apple e banana neste caso)
#Fim


#Adicionando itens a lista
thislist.append("strawberry") # Insere um item no final da lista
print(thislist)

index = 2
item = "Grape"
thislist.insert(index, item) # Insere um item na lista no index especificado (neste caso na posição 2)
print(thislist)

#Fim

#Alterando valor da lista
index = 1
value = "Teste"
thislist[index] = value #Alterando o valor no índice indicado
print(thislist)
#Fim

#Verificando se existe valor na lista
if value in thislist:
    print("Existe")
else:
    print("Não existe")
#fim

#Iterando na lista
for x in thislist:
    print("%s" % x)
    
    """ ou """
p = [x for x in thislist]
print("%s" % p)
#Fim

#Removendo item na lista
thislist.remove("Teste") #Especificando item a ser removido da lista
thislist.pop(index) """ ou """ thislist.pop() 
#Remove o item no índice especificado (Remove o último item se não for especificado nenhum índice)

del thislist[index] # Remove o item no índice espeficado

del thislist #Deleta a lista inteira

thislist.clear() #Limpa a lista inteira
#Fim

#Copiando uma lista
mylist = thislist.copy()
""" ou """
mylist = list(thislist)
#Fim

#Juntar duas listas
listA = ["a", "b", "c"]
listB = ["d", "e", "f"]
listaFinal = listA + listB
print(listaFinal)
#Fim