#Construindo expressões lambdas em Python

x = lambda a : a + 10
print(x(10))

y = lambda a, b: a ** b
print(y(4, 2))

#Fim