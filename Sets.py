#!/usr/bin/env python
# coding: utf-8

# In[1]:


mySet = {"Banana", "Manga", "Morango", "Abacate", "Abacaxi"} #Definindo o Set


# In[2]:


print(mySet) #Não há ordenação certa em sets. Portanto, a ordem será aleatória em todas as vezes


# In[4]:


print ("Manga" in mySet) #Retorna True se houver o valor especificado


# In[5]:


mySet.add("Laranja") #Adicionando valor no set


# In[8]:


mySet.update(["Melancia", "Ameixa", "Uva"]) #Adicionando múltiplos valores ao mesmo tempo


# In[9]:


print(mySet)


# In[10]:


tamanho = len(mySet) #Retorna o tamanho do set


# In[11]:


print(tamanho)


# In[12]:


mySet.remove("Ameixa") #Removendo um valor do set


# In[13]:


mySet


# In[14]:


mySet.discard("Banana") #Remove o valor 'banana' do set


# In[15]:


print(mySet)


# In[16]:


mySet.pop() #Remove o último valor do set


# In[17]:


print(mySet)


# In[18]:


mySet.clear() #Remove todos os itens de dentro do set


# In[19]:


print(mySet)


# In[20]:


del mySet #Remove o set completamente


# In[ ]:




