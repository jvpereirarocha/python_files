# Formatando a string com format
x = "Joao"
txt = "Meu nome é {nome}".format(nome=x)
txt = "Meu nome é {0}".format(x)
txt = "Meu nome é {}".format(x)

#Fim


#Formatando numero com casas decimais
preco = 48
txt = "O preço é R$ {0:.2f} reais".format(preco)
print(txt)
#Fim

#Maneiras de se printar a string

print("meu nome é %s" % x)
print("meu nome é: ", x)
print("meu nome é: " + x)

#Fim