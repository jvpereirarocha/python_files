dicionario = {'descricao': 'qualquer', 'modelo': 'Teste', 'valor': 10.0}


# Função que copia um dicionário
def copy_dict():
    dicionario = {'name': 'Joao', 'age': 21, 'date_of_birth': '27/02/1997'}
    x = dicionario.copy()
    print(x)
# Fim da Função


# Função que atribui um valor a uma ou mais chaves de um dicionário
def from_dict():
    x = ('key1', 'key2', 'key3')
    y = 'Teste'
    thisdict = dict.fromkeys(x, y)
    print(thisdict)
# Fim da Função


# Função que retorna o valor de uma chave especificada
def retorna_valor(dicionario):
    y = dicionario.get('descricao')
    print(y)
# Fim da função


# Função que retorna uma tupla com a chave e o valor de cada registro do dicionário
def retorna_itens(dicionario):
    x = dicionario.items()
    print(x)
# Fim da função


# Função retorna somente as chaves do dicionário (excluindo os valores)
def retorna_chaves(dicionario):
    x = dicionario.keys()
    print(x)
# Fim da função


# Função que remove um registro do dicionário especificado pela função pop()
def remove_registro(dicionario):
    dicionario.pop("modelo")
    print(dicionario)
# Fim da função


# Função que remove o último item do dicionário
def remove_ultimo(dicionario):
    dicionario.popitem()
    print(dicionario)
# Fim da função


# Função que retorna o valor default de uma chave. Cria a chave e seta o valor default caso essa não exista
def setar_padrao(dicionario):
    x = dicionario.setdefault("quantidade", 20)
    print(x)
# Fim da função


# Função atualiza um item do dicionário
def atualizar_item(dicionario):
    dicionario.update({'descricao': 'atualizando'})
    print(dicionario)
# Fim da função


# Função retorna somente os valores do dicionário (excluindo as chaves)
def retorna_values(dicionario):
    x = dicionario.values()
    print(x)
# Fim da função
