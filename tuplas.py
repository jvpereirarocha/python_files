#!/usr/bin/env python
# coding: utf-8

# In[1]:


tupla = ("Teste", "Teste2", "Teste3")


# In[2]:


print(tupla)


# In[3]:


print(tupla[0]) #Acessando primeira posição da tupla


# In[4]:


tupla[1] = "Outro Teste" #ERRO! Não é possível modificar uma tupla


# In[6]:


tupla.index("Teste") #Retorna o index da tupla onde se encontra a pesquisa


# In[8]:


tupla.count("Teste") #Retorna quantas vezes foi encontrada a palavra Teste na tupla especificada


# In[ ]:





# In[ ]:




