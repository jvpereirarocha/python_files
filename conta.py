# -*- coding: utf-8 -*-
from abc import ABC
from collections.abc import MutableSequence
class Conta:
    def __init__(self, numero, titular, saldo, limite=1000.0):
        self.__numero = numero
        self.__titular = titular
        self.__saldo = saldo
        self.__limite = limite
        # self.__codigo_banco = "001"

    def __getitem__(self, item): # Comportamento de uma lista
        return self.__titular[item]

    def __len__(self):
        return len(self.__numero)

    def extrato(self):
        print("Saldo: R$ - {}".format(self.__saldo))

    def deposita(self, valor):
        self.__saldo += valor

    def __pode_sacar(self, valor_a_sacar):
        valor_disponivel = self.__saldo + self.__limite
        return valor_a_sacar <= valor_disponivel

    def saca(self, valor):
        if(self.__pode_sacar(valor)):
            self.__saldo -= valor
        else:
            print("Saldo insuficiente")

    def transfere(self, valor, destino):
        self.saca(valor)
        destino.deposita(valor)

    def get_titular(self):
        return self.__titular

    def get_numero(self):
        return self.__numero

    def get_saldo(self):
        return self.__saldo

    @property
    def limite(self):
        return self.__limite

    @limite.setter
    def limite(self, novoLimite):
        self.__limite = novoLimite

    @staticmethod
    def codigo_banco():
        return "001"


conta = Conta(1, "Teste", 200.0, 5000.0)
conta.extrato()
print(conta.limite)
conta.limite = 2500
print(conta.limite)
print(Conta.codigo_banco())


class Pessoa:
    def __init__(self, nome):
        self.__nome = nome

    @property
    def nome(self):
        return self.__nome.title()

    @nome.setter
    def nome(self, nome):
        self.__nome = nome


class Veiculo:
    def __init__(self, marca, ano, valor):
        self._marca = marca
        self._ano = ano
        self._valor = valor

    @property
    def marca(self):
        return self._marca

    @property
    def valor(self):
        return self._valor

    @valor.setter
    def valor(self, valor):
        self._valor = valor


class Carro(Veiculo):
    def __init__(self, marca, modelo, ano, valor):
        super().__init__(marca, ano, valor)
        self._modelo = modelo


class Moto(Veiculo):
    def __init__(self, modelo, marca, ano, valor):
        super().__init__(marca, ano, valor)
        self.__modelo = modelo

    def __getitem__(self, item):
        return self.__modelo[item]


print("Esse é um teste", end='\n')


class Teste:
    def __init__(self, teste):
        self._teste = teste
